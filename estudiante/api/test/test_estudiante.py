from ..api import app as application
from ..api import connectToDb, parseestudiante
import psycopg2
import pytest
from flask import request
import json

# @pytest.fixture
# def client():
#     app = create_app()
#     app.testing = True
    # # return app
    # client = app.test_client()



def test_parseestudiante():
    data = ('cd9f8fbe-6e0f-437f-87f2-42915291e383', 'Brandon','201503385')
    expected_response = {'pk_estudiante': 'cd9f8fbe-6e0f-437f-87f2-42915291e383', 'nombre': 'Brandon', 'carnet': '201503385'}
    assert parseestudiante(data) == expected_response


def test_404api():
    app = application
    app.testing = True
    with app.test_client() as c:
        response =c.get('/url/not/found')
        assert  response.status=='404 NOT FOUND'


def test_rootapi():
    app = application
    app.testing = True
    with app.test_client() as c:
        response = c.get('/')
        assert response.status == '200 OK'

def create_vaccine(client, data):
    return client.post('/estudiante/new/', data=json.dumps(data))


def test_createvaccine():
    app = application
    # comentario
    app.testing = True
    with app.test_client() as client:
        rv = create_vaccine(client, {'pk_estudiante': 'cd9f8fbe-6e0f-437f-87f2-42915291e383', 'nombre': 'Brandon', 'carnet': '201503385'})
        assert rv.status == '200 OK'

        
        


