from werkzeug.exceptions import NotFound
from flask import Flask
from flask import request
import psycopg2
import uuid
from flask_cors import CORS, cross_origin
import json

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

print("Saludos como estan todos")

def connectToDb(database):
    """Regresa una conexion a la base de datos"""
    conn = psycopg2.connect(
        host="estudiantes_db",
        database="estudiantes_db",
        user="root",
        password="root",
        port="5432"
    )
    return conn


def parseestudiante(data):
    """Retorna los datos parseados de la estudiante"""
    return  {
        "pk_estudiante": data[0],
        "nombre": data[1],
        "carnet": data[2]
    }

@app.route('/test', methods=['GET'])
@cross_origin()
def testAlive():
    return {
        "statusCode": 200,
        "msg": "Saludos"
    }


@app.route('/', methods=['GET'])
@cross_origin()
def estudiantesList():
    """Regresa la lista de todas las estudiantes"""
    response = {"estudiantes":[]}
    conn = None
    try:
        conn = connectToDb("estudiantes_db")
        # raise psycopg2.DatabaseError
        cur = conn.cursor()
        cur.execute("SELECT * FROM estudiante")
        print("The number of parts: ", cur.rowcount)
        estudiante = cur.fetchone()
        while estudiante is not None:
            response["estudiantes"].append(parseestudiante(estudiante))
            estudiante = cur.fetchone()
        cur.close()
        return {
            "statusCode": 200,
            "body": response
        }
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        return {
            "statusCode": 500,
            "body": { "msg": "Internal Server Error", "error": "{0}".format(error) }
        }
    finally:
        if conn is not None:
            conn.close()    


@app.route('/estudiante/new/', methods=['POST'])
@cross_origin()
def newVestudiante():
    """ Inserta una nueva estudiante en la base de datos"""
    if request.method == 'POST':
        sql = """INSERT INTO estudiante(pk_estudiante, nombre, carnet)
                VALUES (%s, %s, %s);"""
        conn = None
        estudiante_id = None
        jsondata = json.loads(request.data)
        try:
            data = (
                "{0}".format(uuid.uuid4()),
                jsondata["nombre"],
                jsondata["carnet"]
            )    
            conn = connectToDb("estudiantes_db")
            cur = conn.cursor()
            cur.execute(sql, data)
            conn.commit()
            cur.close()
            return {
                "statusCode":201,
                "payload": jsondata
            }
        except psycopg2.DatabaseError as error:
            return {
                "statusCode":500,
                "msg": "Ha ocurrido un error al insertar el nuevo registro", 
                "error": "{}".format(error)
            }
        except KeyError as error: 
            return {
                "statusCode":400,
                "msg": "Todos los datos son necesarios y obligatorios", 
                "error": "{}".format(error)
            }
        finally:
            if conn is not None:
                conn.close()     
        

@app.route('/estudiante/<uuid:pk_estudiante>', methods=['GET'])
@cross_origin()
def retrieveestudiante(pk_estudiante):
    """Retorna una estudiante especifica"""
    response = {}
    try:
        conn = connectToDb("estudiantes_db")
        cur = conn.cursor()
        cur.execute("SELECT * FROM estudiante WHERE pk_estudiante = '{0}' ORDER BY nombre;".format(pk_estudiante))
        estudiante = cur.fetchone()
        if estudiante:
            response = parseestudiante(estudiante)
            estudiante = cur.fetchone()
        else:
            raise NotFound
        cur.close()
        return {
            "statusCode": 200,
            "estudiante": response
        }
    except psycopg2.DatabaseError as error:
        return {
            "statusCode": 500,
             "msg": "Internal Server Error", 
             "error": "{0}".format(error) 
        }
    except NotFound as error:
        return {
            "statusCode": 404,
             "msg": "Internal Server Error", 
             "error": "Record Not Found" 
        }
    finally:
        if conn is not None:
            conn.close()

@app.route('/update/<uuid:pk_estudiante>', methods=['PATCH'])
@cross_origin()
def updateEstudiante(pk_estudiante):
    """Retorna una estudiante especifica"""
    response = {}
    try:
        conn = connectToDb("estudiantes_db")
        cur = conn.cursor()
        jsondata = json.loads(request.data)
        sql = "UPDATE estudiante set nombre=%s, carnet=%s where pk_estudiante = %s"
        cur.execute(sql, (jsondata["nombre"], jsondata["carnet"], str(pk_estudiante)))
        # get the number of updated rows
        updated_rows = cur.rowcount
        # Commit the changes to the database
        conn.commit()
        # Close communication with the PostgreSQL database
        cur.close()
        return {
            "statusCode": 200,
            "msg": "ok"
        }
    except psycopg2.DatabaseError as error:
        return {
            "statusCode": 500,
             "msg": "Internal Server Error", 
             "error": "{0}".format(error) 
        }
    except NotFound as error:
        return {
            "statusCode": 404,
             "msg": "Internal Server Error", 
             "error": "Record Not Found" 
        }
    finally:
        if conn is not None:
            conn.close()

@app.route('/delete/<uuid:pk_estudiante>', methods=['DELETE'])
@cross_origin()
def deleteEstudiante(pk_estudiante):
    """Retorna una estudiante especifica"""
    response = {}
    try:
        conn = connectToDb("estudiantes_db")
        cur = conn.cursor()
        jsondata = json.loads(request.data)
        sql = "DELETE from estudiante where pk_estudiante = %s"
        cur.execute(sql,( str(pk_estudiante)))
        # get the number of updated rows
        rows_deleted = cur.rowcount
        # Commit the changes to the database
        conn.commit()
        # Close communication with the PostgreSQL database
        cur.close()
        return {
            "statusCode": 200,
            "msg": "ok"
        }
    except psycopg2.DatabaseError as error:
        return {
            "statusCode": 500,
             "msg": "Internal Server Error", 
             "error": "{0}".format(error) 
        }
    except NotFound as error:
        return {
            "statusCode": 404,
             "msg": "Internal Server Error", 
             "error": "Record Not Found" 
        }
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000,debug=True)